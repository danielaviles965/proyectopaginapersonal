import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getAuth, signInWithEmailAndPassword, onAuthStateChanged } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js";

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyDour5XAkPuE14cc9B2BP3UZ9Ma2hLlYAk",
    authDomain: "final-5a560.firebaseapp.com",
    projectId: "final-5a560",
    storageBucket: "final-5a560.appspot.com",
    messagingSenderId: "734231871836",
    appId: "1:734231871836:web:4c12a8433298fa16d84199"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);

// Referencias a los elementos del DOM
const emailInput = document.getElementById("email");
const passwordInput = document.getElementById("contra");
const loginButton = document.getElementById("btnEnviar");
const errorMessage = document.getElementById("error");

// Escuchar el evento de click en el botón de ingreso
loginButton.addEventListener("click", (e) => {
    e.preventDefault(); // Prevenir el comportamiento por defecto de enviar el formulario

    const email = emailInput.value;
    const password = passwordInput.value;

    // Autenticar al usuario
    signInWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
            // Inicio de sesión exitoso
            console.log("¡Inicio de sesión exitoso!");
            // Redireccionar al usuario a la página principal o a donde necesites
            window.location.href = '../html/menu.html';
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            console.log(`Error ${errorCode}: ${errorMessage}`);
            // Mostrar mensaje de error al usuario
            const errors = document.querySelector('.error');
            errors.classList.add('errors')
            setTimeout(() => {
                errors.classList.remove('errors');
            }, 2000);
            document.querySelector(".error").textContent = "Intenta de nuevo";
        });
});

// Observar el estado de autenticación
onAuthStateChanged(auth, (user) => {
    if (user) {
        // Usuario está autenticado
        console.log("Usuario autenticado:", user);
        // Puedes redireccionar al usuario a otra página o cargar contenido exclusivo para usuarios autenticados
    } else {
        // Usuario no está autenticado
        console.log("Usuario no autenticado");
    }
});
