import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getDatabase, ref, set, onChildAdded, onChildRemoved, remove, update, onChildChanged, onValue } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js';
import { getStorage, ref as storageRef, uploadBytesResumable, getDownloadURL } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js';

// Configuración de Firebase
const firebaseConfig = {
    apiKey: "AIzaSyDour5XAkPuE14cc9B2BP3UZ9Ma2hLlYAk",
    authDomain: "final-5a560.firebaseapp.com",
    projectId: "final-5a560",
    storageBucket: "final-5a560.appspot.com",
    messagingSenderId: "734231871836",
    appId: "1:734231871836:web:4c12a8433298fa16d84199"
};

const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage(app);



// La función deleteProduct ya se proporcionó previamente.
function displayGam() {
    // Crear contenedor principal y añadirlo al DOM
    const divContainer = document.createElement("div");

    document.getElementById('productList').appendChild(divContainer);

    const productsRef = ref(db, 'video');

    // Añadir productos como elementos div individuales dentro del contenedor principal
    onChildAdded(productsRef, snapshot => {
        console.log(`Añadiendo producto con ID ${snapshot.key}`);
        const data = snapshot.val();

        // Crear div para cada propiedad del producto y añadirlos a un div de producto
        const productDiv = document.createElement("div");
        productDiv.classList.add('product');
        productDiv.id = `product-${snapshot.key}`; // ID único para el div del producto

        // Imagen del producto
        const imageDiv = document.createElement("div");
        imageDiv.classList.add('hei');
        const img = document.createElement("img");
        img.classList.add('img');
        imageDiv.classList.add('img_div');
        img.src = data.imageUrl;
        img.alt = data.nombre;
        img.width = 100;
        imageDiv.appendChild(img);
        productDiv.appendChild(imageDiv);

        // Nombre del producto
        const nameDiv = document.createElement("div");
        const h1 = document.createElement('h1');
        h1.classList.add('h1');
        h1.textContent = `${data.nombre}`;
        nameDiv.appendChild(h1);
        productDiv.appendChild(nameDiv);

        // Precio del producto
        const priceDiv = document.createElement("div");
        const p = document.createElement('p');
        p.textContent = `${data.precio}`;
        priceDiv.appendChild(p);
        productDiv.appendChild(priceDiv);

        // Descripción del producto
        const descriptionDiv = document.createElement("div");
        const p2 = document.createElement('p');
        p2.textContent = `${data.descripcion}`;
        descriptionDiv.appendChild(p2);
        productDiv.appendChild(descriptionDiv);

        // Cantidad del producto
        const quantityDiv = document.createElement("div");
        const p3 = document.createElement('p');
        p3.textContent = `${data.cantidad}`;
        quantityDiv.appendChild(p3);
        productDiv.appendChild(quantityDiv);



        // Acciones del producto (botones)
        const actionsDiv = document.createElement("div");


        // Botón Eliminar


        productDiv.appendChild(actionsDiv);
        divContainer.classList.add('add');
        divContainer.classList.add('grid')
        divContainer.classList.add('mar');

        // Añadir el div del producto al contenedor principal
        divContainer.appendChild(productDiv);
    });

    // Deberías manejar onChildChanged y onChildRemoved de manera similar, actualizando o eliminando
    // los divs de productos correspondientes.



    // Actualizar productos
    onChildChanged(productsRef, snapshot => {
        console.log(`Actualizando producto con ID ${snapshot.key}`);
        const data = snapshot.val();
        updateProductInTable(`product-${snapshot.key}`, data);
    });

    // Eliminar productos
    onChildRemoved(productsRef, snapshot => {
        const trToRemove = document.getElementById(`product-${snapshot.key}`);
        if (trToRemove) {
            trToRemove.remove();
        }
    });
}


// Llama a la función para mostrar los productos y habilitar la edición y eliminación en tiempo real
displayGam();
